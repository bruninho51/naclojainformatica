<?php
    include_once("admin/Conexao.php");

    $pesquisa;
    $resultado;
    $dadosProd;

    if (!empty($_GET) && $_GET['pesquisa'] != "") {
        $pesquisa = $_GET['pesquisa'];
        $resultado = 'Resultado da busca por: ' . $pesquisa;
        $sql_busca = "SELECT P.id, P.img, P.nome, P.preco FROM produto P where P.nome LIKE '%$pesquisa%'";
    } else {
        $resultado = "Pesquisa vazia.";
        $sql_busca = "SELECT P.id, P.img, P.nome, P.preco FROM produto P";
    }

    //OBTÉM OS PRODUTOS COM BASE NA BUSCA
    $stmt = $con->prepare($sql_busca);
    //$stmt->bind_param('s', $pesquisa);
    $stmt->execute();
    $dadosProd = $stmt->get_result();

?>
<?php include_once('template/header.php')?>
            <?php include_once("template/busca.php"); ?>
            <h2 class="resultado-busca"><?php echo $resultado; ?></h2>

            <section id="containerProdutos">
                <?php while($prod = $dadosProd->fetch_assoc()) : ?>
                <div class="row">
                    <div class="container-produto container-produto-busca">
                        <div class="img">
                            <img class="img" alt="imagem produto" src="imagens/<?= $prod['img']?>">
                        </div>
                        <a href="Produto.php?prod=<?= $prod['id']?>"><h1 class="nome"><?= $prod['nome']?></h1></a>
                        <strong class="preco"><?= 'R$ ' . number_format($prod['preco'], 2, ',', '.')?></strong>
                    </div>
                </div>    
                <?php endwhile?>
            </section>
            
<?php include_once('template/footer.php')?>
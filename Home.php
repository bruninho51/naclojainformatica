<?php
    include_once("admin/Conexao.php");

    $countProdutoLinha = 1;

    if (!isset($categoria)) {
        $titulo = "Home";
        //OBTÉM 4 PRODUTOS DE FORMA RANDÔMICA, INDEPENDENTEMENTE DA CATEGORIA
        $dadosProd = $con->query(PRODUTO_RANDOM);
    } else {
        //OBTÉM OS 4 PRODUTOS DE MESMA CATEGORIA DE FORMA RANDÔMICA
        $stmt = $con->prepare(PRODUTO_CATEG);
        $stmt->bind_param('s', $_GET['categ']);
        $stmt->execute();
        $dadosProd = $stmt->get_result();
        
        //OBTÉM NOME DA CATEGORIA
        $stmt = $con->prepare(CATEGORIA);
        $stmt->bind_param('s', $_GET['categ']);
        $stmt->execute();
        $stmt->bind_result($nomeCateg);
        $stmt->fetch();
        $titulo = "Categoria - ".$nomeCateg;
    }

?>
<?php include_once('template/header.php')?>
            <?php include_once("template/busca.php"); ?>
            <section id="containerProdutos">
                <?php while($prod = $dadosProd->fetch_assoc()) : ?>
                <div class="row">
                    <div class="container-produto">
                        <div class="img">
                            <img class="img" alt="imagem produto" src="imagens/<?= $prod['img']?>">
                        </div>
                        <a href="Produto.php?prod=<?= $prod['id']?>"><h1 class="nome"><?= $prod['nome']?></h1></a>
                        <p class="descricao"><?= substr($prod['descricao'], 0, 200).'...'?></p>
                        <strong class="preco"><?= 'R$ ' . number_format($prod['preco'], 2, ',', '.')?></strong>
                    </div>
                    <?php if ($prod = $dadosProd->fetch_assoc()) : ?>
                    <div class="container-produto">
                        <div class="img">
                            <img class="img" alt="imagem produto" src="imagens/<?= $prod['img']?>">
                        </div>
                        <a href="Produto.php?prod=<?= $prod['id']?>"><h1 class="nome"><?= $prod['nome']?></h1></a>
                        <p class="descricao"><?= substr($prod['descricao'], 0, 200).'...'?></p>
                        <strong class="preco"><?= 'R$ ' . number_format($prod['preco'], 2, ',', '.')?></strong>
                    </div>
                    <?php endif?>
                </div>    
                <?php endwhile?>
            </section>
<?php include_once('template/footer.php')?>
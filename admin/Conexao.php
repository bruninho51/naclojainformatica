<?php
    define('SERVIDOR', '127.0.0.1');
    define('USUARIO', 'root');
    define('SENHA', '');
    define('BANCO', 'loja_informatica');

    define('PRODUTO_RANDOM', 'SELECT P.id, P.img, P.nome, P.preco, P.descricao, C.nome categoria FROM produto P INNER JOIN categoria_produto C ON C.id = P.categoria ORDER BY rand() LIMIT 4');

    define('PRODUTO_CATEG', 'SELECT P.id, P.img, P.nome, P.preco, P.descricao, C.nome categoria FROM produto P INNER JOIN categoria_produto C ON C.id = P.categoria WHERE P.categoria = ? ORDER BY rand() LIMIT 4');

    define('CATEGORIA', 'SELECT nome FROM categoria_produto WHERE id = ?');

    define('PRODUTO', 'SELECT P.id, P.img, P.nome, P.preco, P.descricao, C.nome categoria, P.categoria idCategoria, P.img_large FROM produto P INNER JOIN categoria_produto C ON C.id = P.categoria WHERE P.id = ?');

    $con = new Mysqli(SERVIDOR, USUARIO, SENHA, BANCO);
    $con->set_charset("utf8");

    
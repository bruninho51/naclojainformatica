<?php
    include_once("../admin/Conexao.php");

    setlocale(LC_ALL, "pt_BR", "pt_BR.iso-8859-1", "pt_BR.utf-8", "portuguese");
	date_default_timezone_set('America/Sao_Paulo');
    $data = strftime("%d de %B de %Y - %H:%m");
    $login = false;

    if (isset($_POST['usuario']) && $_POST['usuario'] != "") {
        $login = true;
        header("location:ListarProdutos.php");
    }
?>
<?php include_once('../template/header.php')?>
            
            <section id="login">
                <h1>Área Restrita</h1>
                <form method="POST">
                    <div class="row center">
                        <label for="usuario">Usuário:</label>
                        <input type="text" name="usuario">
                    </div>
                    <div class="row center">
                        <label for="senha">Senha:<?= str_repeat('&nbsp;', 3)?></label>
                        <input type="password" name="senha">
                    </div>
                    <input id="btnEntrar" type="submit" value="Entrar">
                    
                </form>
            </section>
<?php include_once('../template/footer.php')?>

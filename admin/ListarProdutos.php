<?php
    $login = true;

    include_once("Conexao.php");

    
    $sql = "SELECT P.id, P.nome AS 'produto', C.nome AS 'categoria', P.preco from produto P INNER JOIN categoria_produto C ON C.id = P.categoria";
    //OBTÉM Todos produtos
    $dadosProd = $con->query($sql);
        
?>

<?php include_once('../template/header.php')?>

<section id="containerProdutos">
    <div id="adm-loja">
        <a>Administração da loja</a>
        <table>
            <thead><th>Produtos</td></thead>
            <tr>
                <td>Listar produtos</td>
            </tr>
            <tr>
                <td>Inserir novo produto</td>
            </tr>
            <tr>
                <td>Alterar produto</td>
            </tr>
        </table>
    </div>
    <div id="lista-produtos">
        <p>Lista de produtos</p>
        <table>
            <thead>
                <th>Código</th>
                <th>Nome do produto</th>
                <th>Categoria</th>
                <th>Preço</th>
            </thead>
            <tbody>
                <?php while($prod = $dadosProd->fetch_assoc()) : ?>
                   <tr>
                       <td><?= $prod['id']?></td>
                       <td><?= substr($prod['produto'], 0, 25)?></td>
                       <td><?= $prod['categoria']?></td>
                       <td><?= $prod['preco']?></td>
                       <td>
                           <form method="POST" action="AlterarProduto.php">
                                <input type="hidden" value="<?= $prod['id']?>" name="id">
                                <input type="submit" value="alterar">
                            </form>
                       </td>
                       <td>
                            <form method="POST" action="ExcluirProduto.php">
                                <input type="hidden" value="<?= $prod['id']?>" name="id">
                                <input type="submit" value="excluir">
                            </form>
                       </td>
                   </tr>
                <?php endwhile?>
            </tbody>
        </table>
    </div>
</section>

<?php include_once('../template/footer.php')?>
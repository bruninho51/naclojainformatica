-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: 17-Out-2018 às 02:02
-- Versão do servidor: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `loja_informatica`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `categoria_produto`
--

CREATE TABLE `categoria_produto` (
  `id` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `categoria_produto`
--

INSERT INTO `categoria_produto` (`id`, `nome`) VALUES
(1, 'Computadores'),
(2, 'Placas de Vídeo'),
(3, 'Monitores'),
(4, 'Periféricos');

-- --------------------------------------------------------

--
-- Estrutura da tabela `produto`
--

CREATE TABLE `produto` (
  `id` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `preco` double NOT NULL,
  `descricao` text NOT NULL,
  `categoria` int(11) NOT NULL,
  `img` text,
  `img_large` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `produto`
--

INSERT INTO `produto` (`id`, `nome`, `preco`, `descricao`, `categoria`, `img`, `img_large`) VALUES
(1, 'Mini PC Computador TV Box V88 4K Android 8Gb + Com', 174.95, '<p>Mini PC Computador TV Box V88 4K Android 8Gb + Comando Sem Fios + Cabo HDMI + 4 Portas USB Equipado com um poderoso CPU: RK3229 Quad-Core ArmCortex a 1.5GHz.</p><p>A Android TV Box SCISHION V88 4K pode ser utilizada como TV Box (“Kodi Box”) ou como um computador de secretária ultra-compacto.</p>', 1, 'mini_pc.jpg', 'mini_pc_large.png'),
(2, 'PLACA DE VIDEO GTX 1070 EX GEFORCE GALAX 70NSH6DHL', 3256.87, 'PLACA DE VIDEO GTX 1070 EX GEFORCE GALAX 70NSH6DHL4XE 8GB DDR5 256BIT 8000MHZ DVI HDMI DP', 2, 'placa_video_1.jpg', 'placa_video_1_large.png'),
(3, 'MONITOR AOC 15.6 LCD LED WIDESCREEN E1670SWU (ALIM', 436.16, ' Experiência visual impressionante e economia para você! O monitor AOC E1670SWU traz tecnologia de última geração para assegurar imagens de alto nível e baixo consumo de energia. O produto assegura um desempenho econômico em seu modo Low Power e com funções como Off Timer, Eco Mode e e-Saver. Além de ter resolução HD e formato widescreen, ele oferece taxa de contraste elevada e , o que resulta em cores muito mais vivas e nítidas. Ele também vem com o exclusivo software Screen +, o qual possibilita que a tela seja dividida em várias janelas. Como a alimentação de energia se dá pela porta USB, você não precisa ligar o aparelho à rede elétrica – muito mais conveniência para você! Além de muito bonito, o acabamento frontal em aço escovado e a textura em fibras entrelaçadas na traseira conferem longa durabilidade ao produto. E para completar, seu desig permite a você liberar mais espaço na mesa.', 3, 'monitor_1.jpg', 'monitor_1_large.png'),
(4, 'Mouse Sem Fio 2,4 GHz com USB Multilaser - Preto', 59.99, 'O Mouse Sem Fio 2,4 GHz com USB Multilaser possui design anatômico e textura emborrachada para seu conforto, além de ser compacto e colorido, ideal para quem busca estilo e qualidade. Super prático, não tem conexão com fios, facilitando o uso.', 4, 'mouse_1.jpg', 'mouse_1_large.png'),
(5, 'MONITOR AOC 23.6 LED M2470SWD2 FHD/DVI', 798.77, 'MONITOR AOC 23,6  LED M2470SWD2 FHD/DVI', 3, 'monitor_2.jpg', 'monitor_2_large.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categoria_produto`
--
ALTER TABLE `categoria_produto`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `produto`
--
ALTER TABLE `produto`
  ADD PRIMARY KEY (`id`),
  ADD KEY `c_categ` (`categoria`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categoria_produto`
--
ALTER TABLE `categoria_produto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `produto`
--
ALTER TABLE `produto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `produto`
--
ALTER TABLE `produto`
  ADD CONSTRAINT `c_categ` FOREIGN KEY (`categoria`) REFERENCES `categoria_produto` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

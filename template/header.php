<?php
    setlocale(LC_ALL, "pt_BR", "pt_BR.iso-8859-1", "pt_BR.utf-8", "portuguese");
	date_default_timezone_set('America/Sao_Paulo');
    $data = strftime("%d de %B de %Y - %H:%m");
?>
<!DOCTYPE html>
<html>
    <head>
        <title><?= $titulo?></title>
        <meta charset="utf-8">
        <link rel="stylesheet" href="https://<?= $_SERVER['HTTP_HOST']?>/css/estilo.css?<?php echo time()?>">
    </head>
    <body>
        <div class="wrap-principal">
            <small id="dataHoje"><?= $data?></small>
            <nav id="navPrincipal">
                <div onclick="window.location.href = 'https://<?= $_SERVER['HTTP_HOST']?>/index.php'"><img id="brand" alt="LOGOTIPO" src="https://<?= $_SERVER['HTTP_HOST']?>/imagens/logo.png"></div>
                <?php if (!isset($login)) : ?>
                <ul>
                    <li><a href="Categoria.php?categ=1">Computadores</a></li>
                    <li><a href="Categoria.php?categ=2">Placas de Vídeo</a></li>
                    <li><a href="Categoria.php?categ=3">Monitores</a></li>
                    <li><a href="Categoria.php?categ=4">Periféricos</a></li>
                </ul>
                <?php endif?>
                
            </nav>
            <?php if (isset($categoria)) : ?>
            <div id="navegador">
                <a href="index.php">HOME</a> > <?= $nomeCateg?>
            </div>
            <?php elseif (isset($produto)) : ?>
            <div id="navegador">
                <a href="index.php">HOME</a> > 
                <a href="Categoria.php?categ=<?= $categoriaProdId?>"><?= $categoriaProd?></a> > <?= $nomeProd?>
                
            </div>
            <?php endif?>
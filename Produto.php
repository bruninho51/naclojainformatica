<?php
    include_once("admin/Conexao.php");
    $produto = true;

    
    //OBTÉM OS DADOS DO PRODUTO
    if (isset($_GET['prod']) && !empty($_GET['prod'])) {
        $stmt = $con->prepare(PRODUTO);
        $stmt->bind_param('s', $_GET['prod']);
        $stmt->execute();
        $dadosProd = $stmt->bind_result($idProd, $imgProd, $nomeProd, $precoProd, $descricaoProd, $categoriaProd, $categoriaProdId, $imgLarge);
        $stmt->fetch();
        $titulo = $nomeProd;
        
    } else {
        http_response_code(404);
        die();
    }
    
    

?>
<?php include_once('template/header.php')?>
            <?php include_once("template/busca.php"); ?>
            <div class="row">
                <div id="container-produto-info">
                    <div id="img-grande-produto">
                        <img class="img-grande" src="imagens/<?= $imgLarge?>" alt="produto imagem">
                    </div>
                    <div id="container-flex">
                        <div id="info-produto">
                            <h1 class="nome"><?= $nomeProd?></h1>
                            <p class="descricao"><?= $descricaoProd?></p>
                            <div class="preco-btn-voltar">
                                <strong class="preco"><?= 'R$ ' . number_format($precoProd, 2, ',', '.')?></strong>
                                <button onclick="window.location.href = 'index.php'" class="btn-voltar">Voltar</button>
                            </div>
                        </div>
                        <div class="img_produto-info">
                            <div class="img">
                                <img class="img" alt="imagem produto" src="imagens/<?= $imgProd?>">
                            </div>
                            <div class="img">
                                <img class="img" alt="imagem produto" src="imagens/<?= $imgProd?>">
                            </div>
                            <div class="img">
                                <img class="img" alt="imagem produto" src="imagens/<?= $imgProd?>">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<?php include_once('template/footer.php')?>